<?php

namespace meteam\TildaBundle\Service\Tilda;



use meteam\TildaBundle\Entity\TildaStaticPage;
use meteam\TildaBundle\Service\User\UserService;
use Psr\Container\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Psr\Log\LoggerInterface;
use Doctrine\ORM\EntityManagerInterface;
use meteam\TildaBundle\Repository\TildaStaticPageRepository;
use meteam\TildaBundle\Repository\TildaStaticPageTypeRepository;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Finder\Finder;

class SaveTildaPageService
{
    const DEFAULT_LOCALE = 'en';
    const DEFAULT_URL = 'default';
    /**
     * @var EntityManagerInterface
     */
    private $em;
    /**
     * @var TildaStaticPageRepository
     */
    private $staticPageRepository;
    /**
     * @var TildaStaticPageTypeRepository
     */
    private $tildaStaticPageTypeRepository;
    /**
     * @var ParameterBagInterface
     */
    private $parameterBag;
    /**
     * @var LoggerInterface
     */
    private $logger;
    /**
     * @var RequestStack
     */
    private $request;
    /**
     * @var UserService
     */
    private $userService;
    /**
     * @var ContainerInterface
     */
    private $container;

    public function __construct(
        EntityManagerInterface $em,
        TildaStaticPageRepository $staticPageRepository,
        TildaStaticPageTypeRepository $tildaStaticPageTypeRepository,
        UserService $userService,
        ParameterBagInterface $parameterBag,
        LoggerInterface $logger,
        RequestStack $requestStack,
        ContainerInterface $container
    )
    {
        $this->em = $em;
        $this->staticPageRepository = $staticPageRepository;
        $this->tildaStaticPageTypeRepository = $tildaStaticPageTypeRepository;
        $this->parameterBag = $parameterBag;
        $this->logger = $logger;
        $this->request = $requestStack->getCurrentRequest();
        $this->userService = $userService;
        $this->container = $container;
    }

    public function getPageContent(string $urlType, string $urlPage){
        $staticPageType = $this->tildaStaticPageTypeRepository->findOneBy(['url' => $urlType]);

//        if (!$staticPageType){
//            $this->logger->error('There is no' . $urlType . 'tilda type.');
//            return new RedirectResponse('/');
//        }

        $page = $this->staticPageRepository->findOneBy(['url' => $urlPage, 'type' => $staticPageType, 'locale' => $this->request->getLocale()]);
        if (!$page){
            $page = $this->staticPageRepository->findOneBy(['url' => $urlPage, 'type' => $staticPageType, 'locale' => self::DEFAULT_LOCALE]);
        }
        if (!$page){
            $page = $this->staticPageRepository->findOneBy(['url' => $urlPage, 'type' => $staticPageType]);
        }

        if (!$page){
//            $this->logger->error('There is no such' . $urlPage . 'tilda page.');
//            return new RedirectResponse('/');
            return null;
        }

        if (!$page->getToShow() && !$this->userService->isAdmin()){
//            $this->logger->alert('Page can not be open.' . $staticPageType->getUrl() . "/" . $page->getUrl());
//            return new RedirectResponse('/');
            return null;
        }

        $files = $this->getCssAndJsPaths($page);

        return ['page' => $page, 'files' => $files];
    }

    public function getPageResponse(string $urlType, string $urlPage)
    {
        $content = $this->getPageContent($urlType, $urlPage);
        if (is_array($content)){
            return $this->container->get('twig')->render('staticPage/tildaStaticPage.html.twig',$content);
        }
//        return new RedirectResponse('/');
        return null;
    }
    public function getCheckPageResponse(string $urlType, string $urlPage)
    {
        $content = $this->getPageContent($urlType, $urlPage);
        if (is_array($content)){
            return $this->container->get('twig')->render('staticPage/checkTildaStaticPage.twig',$content);
        }
//        return new RedirectResponse('/');
        return null;
    }

    public function saveOnePage(TildaApi $api, int $pageId, LocalProject $local)
    {
        $tildapage = $api->getPageFullExport($pageId);

        if (! $tildapage || empty($tildapage['html'])) {
            echo "Error: cannot get page [$pageId] or page not publish\n";
        }
        $html = preg_replace(array('|//static.tildacdn.com/js/|','|//static.tildacdn.com/css/|'),array('',''), $tildapage['html']);
        if ($html > '') {
            $tildapage['html'] = $html;
        }
        $tildapage['export_imgpath'] = $local->arProject['export_imgpath'];
        $tildapage['needsync'] = '0';

        /* так как мы копировали общие файлы в одни папки, а в HTML они указывают на другие, то произведем замену */
        $html = preg_replace($local->arSearchFiles, $local->arReplaceFiles, $tildapage['html']);
        if ($html > '') {
            $tildapage['html'] = $html;
        }
        /* сохраним страницу  (при сохранении также происходит копирование картинок использованных на странице) */
        $tildapage = $local->savePage($tildapage);

        return $tildapage;
        //todo
        $tildapage = $local->saveMetaPage($tildapage);
    }

    public function getGeneralProjectFiles(TildaApi $api, int $projectId)
    {
        $local = new LocalProject(array(
                'projectDir' => 'tilda',
                /*
                 'buglovers' => 'dev@example.ru', // email for send mail with error or exception
                 'baseDir' => '/var/www/example.ru/'  //  absolute path for sites files
                */
            )
        );

        if ($local->createBaseFolders() === false) {
            die("Error for create folders\n");
        }

        $arProject = $api->getProjectExport($projectId);

        if (!$arProject) {
            die('Not found project [' . $api->lastError . ']');
        }

        $local->setProject($arProject);

        /* копируем общие CSS файлы */
        $arFiles = $local->copyCssFiles('css');
        if (!$arFiles) {
            die('Error in copy CSS files [' . $api->lastError . ']');
        }

        /* копируем общие JS файлы */
        $arFiles = $local->copyJsFiles('js');
        if (!$arFiles) {
            die('Error in copy JS files [' . $api->lastError . ']');
        }

        /* копируем общие IMG файлы */
        $arFiles = $local->copyImagesFiles('img');
        if (!$arFiles) {
            die('Error in copy IMG files [' . $api->lastError . ']');
        }

        return $local;
    }

    public function savePageToDB(array $tildapage, int $pageId, int $projectid)
    {
        $type = $this->tildaStaticPageTypeRepository->findAll()[0] ?? null;//default type
        if (null === $type){
            return null;
        }
        $tildaPage = $this->staticPageRepository->findOneBy(['pageId' => $pageId, 'projectId' => $projectid]);
        if (!$tildaPage){
            $tildaPage = new TildaStaticPage();
        }
        preg_match('/<body(.*?)<\/body>/s', $tildapage['html'],$match);
        $tildaPage->setBody($match[0]);
        if(!$tildaPage->getUrl()){
            $tildaPage->setUrl((string)$tildapage['id']);
        }
        $tildaPage->getMeta() ?
            $tildaPage->setMeta($tildaPage->getMeta()):
            $tildaPage->setMeta('');
        if(!$tildaPage->getLocale()){
            $tildaPage->setLocale(self::DEFAULT_LOCALE);
        }
        $tildaPage->getToShow() ?
            $tildaPage->setToShow(true):
            $tildaPage->setToShow(false);
        $tildaPage->setPageId($pageId);
        $tildaPage->setProjectId($projectid);
        $tildaPage->getType() ?
            $tildaPage->setType($tildaPage->getType()):
            $tildaPage->setType($type);
        if (!$tildaPage->getId()){
            $this->em->persist($tildaPage);
        }
        $this->em->flush();
        return $type;
    }

    public function getCssAndJsPaths(TildaStaticPage $page)
    {
        $finder = new Finder();

        $files = ['css' => [], 'js' => []];
        $finder->files()->in($this->parameterBag->get('kernel.project_dir') . '/public/tilda/css/')->sortByChangedTime();
        foreach ($finder as $file){
            if (!$this->isValidFileForPage($page, $file)) {
                continue;
            }
            $files['css'][] = $file->getFilename();
        }
        unset($finder);
        $finder = new Finder();
        $finder->files()->in($this->parameterBag->get('kernel.project_dir') . '/public/tilda/js/')->sortByName();
        foreach ($finder as $file){
            if (!$this->isValidFileForPage($page, $file)) {
                continue;
            }
            $files['js'][] = $file->getFilename();
        }
        unset($finder);

        return $files;
    }

    private function isValidFileForPage(TildaStaticPage $page, $file)
    {
        if (str_contains($file->getFilename(), 'tilda-blocks-page')) {
            if (!str_contains($file->getFilename(), $page->getProjectId()) || !str_contains($file->getFilename(), $page->getPageId())) {
                return false;
            }
        }
        return true;
    }
}