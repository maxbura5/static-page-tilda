<?php

namespace meteam\TildaBundle\Service\User;

use App\Entity\User;
use Symfony\Component\Security\Core\Security;
class UserService
{
    /**
     * @var Security
     */
    private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    public function getUser(): ?User
    {
        $user = $this->security->getUser();
        if ($user instanceof User) {
            return $user;
        }
        return null;
    }

    public function isAdmin(): bool
    {
        $user = $this->security->getUser();
        if ($user instanceof User && in_array('ROLE_ADMIN', $user->getRoles())) {
            return true;
        }
        return false;
    }
}