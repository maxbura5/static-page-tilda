<?php

namespace meteam\TildaBundle\Controller;

use meteam\TildaBundle\Entity\TildaStaticPage;
use meteam\TildaBundle\Form\TildaStaticPageTypeType;
use meteam\TildaBundle\Repository\TildaStaticPageTypeRepository;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Psr\Log\LoggerInterface;
use Doctrine\ORM\EntityManagerInterface;
use meteam\TildaBundle\Form\TildaStaticPageType;
use meteam\TildaBundle\Repository\TildaStaticPageRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Finder\Finder;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use meteam\TildaBundle\Service\Tilda;

/**
 * @IsGranted("ROLE_ADMIN")
 * @Route("/tilda")
 */
class TildaController extends AbstractController
{
    const DEFAULT_LOCALE = 'en';
    const DEFULT_IMG_PATH = 'tilda/staticImg';
    private $projectIds;
    private $publicKey;
    private $privateKey;
    /**
     * @var ParameterBagInterface
     */
    private $parameterBag;
    /**
     * @var LoggerInterface
     */
    private $logger;

    public function __construct(
        ParameterBagInterface $parameterBag,
        LoggerInterface $logger
    ) {
        $this->publicKey = $parameterBag->get('tilda.public_key');
        $this->privateKey = $parameterBag->get('tilda.private_key');
        $this->projectIds = $parameterBag->get('tilda.project_ids');
        $this->parameterBag = $parameterBag;
        $this->logger = $logger;
    }

//    /**
//     * @Route("/web-hook")
//     */
//    public function tildaWebHook(Request $request, Tilda\SaveTildaPageService $saveTildaPageService): Response
//    {
//        $pageid = $request->query->get('pageid');
//        $projectid = $request->query->get('projectid');
//
//        $api = new Tilda\TildaApi($this->publicKey, $this->privateKey);
//
//        $local = $saveTildaPageService->getGeneralProjectFiles($api, $projectid);
//
//        $tildaPage = $saveTildaPageService->saveOnePage($api, $pageid, $local);
//
//        $saveTildaPageService->savePageToDB($tildaPage, $pageid, $projectid);
//
//        return new Response('ok', 200);
//    }


    /**
     * @Route("/sync/{projectId}/{pageId}", name="tilda_sync_page")
     */
    public function syncPage(
        int $projectId,
        int $pageId,
        Tilda\SaveTildaPageService $saveTildaPageService,
        TildaStaticPageRepository $pageRepository
    ) {
        $page = $pageRepository->findOneBy(['projectId' => $projectId, 'pageId' => $pageId]);

        if (null === $page) {
            $this->addFlash('error', 'You can not sync this page.');
            return $this->redirectToRoute('tilda_all');
        }

        $api = new Tilda\TildaApi($this->publicKey, $this->privateKey);

        $local = $saveTildaPageService->getGeneralProjectFiles($api, $projectId);

        $tildaPage = $saveTildaPageService->saveOnePage($api, $pageId, $local);
        $type = $saveTildaPageService->savePageToDB($tildaPage, $pageId, $projectId);
        if (!$type) {
            $this->addFlash('error', 'There is no type.');
            return $this->redirectToRoute('tilda_all');
        };


        return $this->redirectToRoute("tilda_all");
    }

    /**
     * @Route("/sync/all", name="tilda_sync_project")
     */
    public function syncAllProject(Tilda\SaveTildaPageService $saveTildaPageService)
    {
        foreach ($this->projectIds as $projectId) {
            $api = new Tilda\TildaApi($this->publicKey, $this->privateKey);

            $local = $saveTildaPageService->getGeneralProjectFiles($api, $projectId);

            $arPages = $api->getPagesList($projectId);

            foreach ($arPages as $arPage) {
                if (null === $this->getDoctrine()->getRepository(TildaStaticPage::class)->findOneBy(
                        ['pageId' => $arPage['id']]
                    )) {
                    $tildaPage = $saveTildaPageService->saveOnePage($api, $arPage['id'], $local);
                    $type = $saveTildaPageService->savePageToDB($tildaPage, $arPage['id'], $projectId);
                    if (!$type) {
                        $this->addFlash('error', 'There is no type.');
                        return $this->redirectToRoute('tilda_all');
                    };
                }
            }
        }

        return $this->redirectToRoute("tilda_all");
    }

    /**
     * @Route("/edit/{id}", name="tilda_edit")
     * @param int $id
     * @param TildaStaticPageRepository $repository
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @return Response
     */
    public function edit(
        int $id,
        TildaStaticPageRepository $repository,
        Request $request,
        EntityManagerInterface $entityManager
    ): Response {
        $tildaPages = $repository->find($id);
        if (!$tildaPages) {
            $tildaPages = new TildaStaticPage();
        }
        $oldFileName = $tildaPages->getPreviewImage();

        $form = $this->createForm(TildaStaticPageType::class, $tildaPages);

        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            $pageImg = $form->get('previewImage')->getData();
            if ($pageImg) {
                $originalFilename = pathinfo($pageImg->getClientOriginalName(), PATHINFO_FILENAME);
                $newFilename = $oldFileName;
                if (!$tildaPages->getPreviewImage()) {
                    $newFilename = $tildaPages->getPageId() . '-' . uniqid() . '.' . $pageImg->guessExtension();
                }
                try {
                    $pageImg->move(
                        self::DEFULT_IMG_PATH,
                        $newFilename
                    );
                } catch (FileException $e) {
                    $this->logger->error($e->getMessage());
                }
                if (!$oldFileName) {
                    $tildaPages->setPreviewImage(self::DEFULT_IMG_PATH . '/' . $newFilename);
                } else {
                    $tildaPages->setPreviewImage($oldFileName);
                }
            }

            $entityManager->flush();

            return $this->redirectToRoute('tilda_all');
        }

        return $this->render("@Tilda/tilda/edit/edit-tilda-page.twig", ['form' => $form->createView()]);
    }

    /**
     * @Route("/edit/type/{id}", name="tilda_type_edit")
     * @param int $id
     * @param TildaStaticPageRepository $repository
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @return Response
     */
    public function editPageType(
        int $id,
        TildaStaticPageTypeRepository $repository,
        Request $request,
        EntityManagerInterface $entityManager
    ): Response {
        $tildaPages = $repository->find($id);

        if (!$tildaPages) {
            $tildaPages = new \meteam\TildaBundle\Entity\TildaStaticPageType();
        }

        $oldFileName = $tildaPages->getPageImg();

        $form = $this->createForm(TildaStaticPageTypeType::class, $tildaPages);

        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            $pageImg = $form->get('page_img')->getData();
            if ($pageImg) {
                $originalFilename = pathinfo($pageImg->getClientOriginalName(), PATHINFO_FILENAME);
                $newFilename = $oldFileName;
                if (!$tildaPages->getPageImg()) {
                    $newFilename = $originalFilename . '-' . uniqid() . '.' . $pageImg->guessExtension();
                }
                try {
                    $pageImg->move(
                        self::DEFULT_IMG_PATH,
                        $newFilename
                    );
                } catch (FileException $e) {
                    $this->logger->error($e->getMessage());
                }
                if (!$oldFileName) {
                    $tildaPages->setPageImg(self::DEFULT_IMG_PATH . '/' . $newFilename);
                } else {
                    $tildaPages->setPageImg($oldFileName);
                }
            }
            if (!$tildaPages->getId()) {
                $entityManager->persist($tildaPages);
            }
            $entityManager->flush();

            return $this->redirectToRoute('tilda_all');
        }

        return $this->render("@Tilda/tilda/editType/edit-tilda-page.twig", ['form' => $form->createView()]);
    }

    /**
     * @Route("/all", name="tilda_all")
     */
    public function getAll(TildaStaticPageRepository $repository, TildaStaticPageTypeRepository $rep): Response
    {
        $tildaPages = array_reverse($repository->findAll());
        $parameterBag = [
            'item' => $this->parameterBag->get('tilda.static_page_item'),
            'list' => $this->parameterBag->get('tilda.static_page_list')
        ];
        $tildaPageTypes = array_reverse($rep->findAll());

        return $this->render(
            "@Tilda/tilda/list/tilda-list.twig",
            compact('tildaPages', 'tildaPageTypes', 'parameterBag')
        );
    }
}