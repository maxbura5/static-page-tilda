<?php

namespace meteam\TildaBundle\Controller;

use meteam\TildaBundle\Service\Tilda\SaveTildaPageService;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use meteam\TildaBundle\Repository\TildaStaticPageRepository;
use meteam\TildaBundle\Repository\TildaStaticPageTypeRepository;
use meteam\TildaBundle\Service\Plerdy\PlerdyEnableChecker;
use Symfony\Component\HttpFoundation\Request;

///**
// * @Route("/page")
// */
class PageController extends AbstractController
{
    /**
     * @var ParameterBagInterface
     */
    private $parameterBag;

    public function __construct(ParameterBagInterface $parameterBag)
    {
        $this->parameterBag = $parameterBag;
    }

    const DEFAULT_LOCALE = 'en';
//    /**
//     * @Route("/{pageType}/{url}", name="tilda_show")
//     */
    public function viewPage(
        string $pageType,
        string $url,
        TildaStaticPageRepository $repository,
        TildaStaticPageTypeRepository $staticPageTypeRepository,
        SaveTildaPageService $saveTildaPageService,
        Request $request
    ) {
        $staticPageType = $staticPageTypeRepository->findOneBy(['url' => $pageType]);

        if (!$pageType){
            $this->logger->error('There is no' . $pageType . 'tilda type.');
            return new RedirectResponse('/');
        }

        $page = $repository->findOneBy(['url' => $url, 'type' => $staticPageType, 'locale' => $request->getLocale()]);
        if (!$page){
            $page = $repository->findOneBy(['url' => $url, 'type' => $staticPageType, 'locale' => self::DEFAULT_LOCALE]);
        }

        if (!$page){
            $this->logger->error('There is no such' . $url . 'tilda page.');
            return new RedirectResponse('/');
        }

        $files = $saveTildaPageService->getCssAndJsPaths();
        $template['footer'] = $this->parameterBag->get('tilda.footer');
        $template['nav_top'] = $this->parameterBag->get('tilda.nav_top');
        $template['nav_left'] = $this->parameterBag->get('tilda.nav_left');
        return $this->render('@Tilda/tilda/test.twig',['page' => $page, 'files' => $files, 'template' => $template]);
    }
}