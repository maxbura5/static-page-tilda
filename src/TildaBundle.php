<?php
namespace meteam\TildaBundle;

use meteam\TildaBundle\DependencyInjection\TildaExtension;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class TildaBundle extends Bundle
{
    public function getContainerExtension()
    {
        return new TildaExtension();
    }
}