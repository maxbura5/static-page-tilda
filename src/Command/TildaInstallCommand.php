<?php

namespace meteam\TildaBundle\Command;

use meteam\TildaBundle\Entity\TildaStaticPageType;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Output\NullOutput;
use Psr\Log\LoggerInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Exception\ExceptionInterface;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;
use Symfony\Component\Filesystem\Filesystem;

class TildaInstallCommand extends Command
{
    protected static $defaultName = 'meteam:tilda:install';
    /**
     * @var ParameterBagInterface
     */
    private $parameterBag;
    /**
     * @var LoggerInterface
     */
    private $logger;
    /**
     * @var Filesystem
     */
    private $filesystem;
    /**
     * @var EntityManagerInterface
     */
    private $em;

    public function __construct(
        $name = null,
        ParameterBagInterface $parameterBag,
        LoggerInterface $logger,
        Filesystem $filesystem,
        EntityManagerInterface $em
    ) {
        parent::__construct($name);
        $this->parameterBag = $parameterBag;
        $this->logger = $logger;
        $this->filesystem = $filesystem;
        $this->em = $em;
    }


    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $command = $this->getApplication()->find('doctrine:migration:diff');

        $greetInput = new ArrayInput([]);

        try {
            $returnCode = $command->run($greetInput, $output);
        } catch (ExceptionInterface $e) {
            $this->logger->error( $e->getMessage());
            $io->error('Installation failed.');
            return Command::FAILURE;
        }

        
        $bundleConfigFilename = $this->parameterBag->get('kernel.project_dir')
            . DIRECTORY_SEPARATOR . 'config'
            . DIRECTORY_SEPARATOR . 'packages'
            . DIRECTORY_SEPARATOR . 'tilda.yaml'
        ;
        $config = <<<YAML
tilda:
        public_key: 8vept7purwyh3et721hd
        private_key: 5qyn3mrbvistmorequ8a
        project_ids: ["4831967", "4868398"]
        static_page_item: static-page-item
        static_page_list: static-page-list
YAML;

        $this->filesystem->appendToFile($bundleConfigFilename, $config);

        $annotationsFile = $this->parameterBag->get('kernel.project_dir')
            . DIRECTORY_SEPARATOR . 'config'
            . DIRECTORY_SEPARATOR . 'routes'
            . DIRECTORY_SEPARATOR . 'annotations.yaml';

        $config = <<<YAML
tilda-contoller:
        resource: ../../vendor/meteam/tilda-bundle/src/Controller/
        type: annotation
YAML;
        $this->filesystem->appendToFile($annotationsFile, $config);

        $controllersFile = $this->parameterBag->get('kernel.project_dir')
            . DIRECTORY_SEPARATOR . 'config'
            . DIRECTORY_SEPARATOR . 'services.yaml';
        $config = <<<YAML
    meteam\TildaBundle\Controller\TildaController:
        class: meteam\TildaBundle\Controller\TildaController
        tags: [ 'controller.service_arguments' ]
    meteam\TildaBundle\Service\Tilda\SaveTildaPageService:
        class: meteam\TildaBundle\Service\Tilda\SaveTildaPageService
        tags: [ 'service_arguments' ]
YAML;
        $this->filesystem->appendToFile($controllersFile, $config);

        $gitignore = $this->parameterBag->get('kernel.project_dir')
            . DIRECTORY_SEPARATOR . '.gitignore';
        $config = '
/public/tilda/';
        $this->filesystem->appendToFile($gitignore, $config);

//        $command = $this->getApplication()->find('doctrine:migrations:migrate');
//
//        $greetInput = new ArrayInput([]);
//
//        try {
//            $returnCode = $command->run($greetInput, $output);
//        } catch (ExceptionInterface $e) {
//            $this->logger->error($e->getMessage());
//            $io->error('Installation failed.');
//            return Command::FAILURE;
//        }



//        $tildaPageStaticType = new TildaStaticPageType();
//        $tildaPageStaticType->setUrl('default');
//        $tildaPageStaticType->setDescription('default');
//        $tildaPageStaticType->setTitle('default');
//        $tildaPageStaticType->setMeta('default');
//        $tildaPageStaticType->setMetaDescription('default');
//        $tildaPageStaticType->setPageHeading('default');
//        $tildaPageStaticType->setTemplate('default');
//        $tildaPageStaticType->setTextPreview('default');
//
//        $this->em->persist($tildaPageStaticType);
//        $this->em->flush();

        $io->success('Installed. Now you have to run `doctrine:migrations:migrate` and then make TildaStaticPageType first entity.');

        return Command::SUCCESS;
    }
}
