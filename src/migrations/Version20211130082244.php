<?php

declare(strict_types=1);

namespace meteam\TildaBundle\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211130082244 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE tilda_static_page (id INT AUTO_INCREMENT NOT NULL, type_id INT NOT NULL, locale VARCHAR(4) DEFAULT NULL, url VARCHAR(255) NOT NULL, body LONGTEXT NOT NULL, page_id VARCHAR(255) NOT NULL, project_id VARCHAR(255) NOT NULL, INDEX IDX_D17AEFB2C54C8C93 (type_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE tilda_static_page_type (id INT AUTO_INCREMENT NOT NULL, title VARCHAR(255) NOT NULL, url VARCHAR(255) NOT NULL, meta VARCHAR(255) NOT NULL, meta_description VARCHAR(255) NOT NULL, description VARCHAR(255) NOT NULL, template VARCHAR(255) NOT NULL, page_heading VARCHAR(255) NOT NULL, text_preview VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE tilda_static_page ADD CONSTRAINT FK_D17AEFB2C54C8C93 FOREIGN KEY (type_id) REFERENCES tilda_static_page_type (id)');
        $this->addSql('ALTER TABLE tilda_static_page ADD alt VARCHAR(255) DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE tilda_static_page DROP FOREIGN KEY FK_D17AEFB2C54C8C93');
        $this->addSql('DROP TABLE tilda_static_page');
        $this->addSql('DROP TABLE tilda_static_page_type');
    }
}
