<?php

namespace meteam\TildaBundle\Repository;

use meteam\TildaBundle\Entity\TildaStaticPage;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method TildaStaticPage|null find($id, $lockMode = null, $lockVersion = null)
 * @method TildaStaticPage|null findOneBy(array $criteria, array $orderBy = null)
 * @method TildaStaticPage[]    findAll()
 * @method TildaStaticPage[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TildaStaticPageRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TildaStaticPage::class);
    }

    /**
     * @param int $typeId
     * @param string $locale
     * @return TildaStaticPage[]
     */
    public function getStaticPagesByTypeId(string $locale = 'en', int $typeId = 0): array
    {
        $qb = $this->createQueryBuilder('tsp')
            ->select()
            ->orderBy('tsp.created', 'DESC')
            ->where('tsp.locale = :locale')
            ->andWhere('tsp.toShow = :toShow')
            ->setParameter('locale', $locale)
            ->setParameter('toShow', 1);
        if (!empty($typeId)) {
            $qb->andWhere('tsp.typeId = :typeId')
                ->setParameter('typeId', $typeId);
        }

        return $qb->getQuery()->getResult();
    }

    public function getTildaStaticPageListByVisibility(int $offset, int $limit): array
    {
        return $this->createQueryBuilder('tsp')
            ->select('tsp')
            ->where('tsp.toShow = 1')
            ->andWhere('tsp.id > :offset')
            ->setParameter('offset', $offset)
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult();
    }
}
