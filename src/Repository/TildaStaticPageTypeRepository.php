<?php

namespace meteam\TildaBundle\Repository;

use meteam\TildaBundle\Entity\TildaStaticPageType;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method TildaStaticPageType|null find($id, $lockMode = null, $lockVersion = null)
 * @method TildaStaticPageType|null findOneBy(array $criteria, array $orderBy = null)
 * @method TildaStaticPageType[]    findAll()
 * @method TildaStaticPageType[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TildaStaticPageTypeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TildaStaticPageType::class);
    }

    // /**
    //  * @return TildaStaticPageType[] Returns an array of TildaStaticPageType objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TildaStaticPageType
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
