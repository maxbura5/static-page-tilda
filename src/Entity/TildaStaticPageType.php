<?php

namespace meteam\TildaBundle\Entity;

use meteam\TildaBundle\Repository\TildaStaticPageTypeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=TildaStaticPageTypeRepository::class)
 */
class TildaStaticPageType
{
    public function __construct()
    {
        $this->tildaStaticPages = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->getUrl();
    }

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $title;

    /**
     * @ORM\OneToMany(targetEntity=TildaStaticPage::class, mappedBy="type", orphanRemoval=true)
     */
    private $tildaStaticPages;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $url;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $meta;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $metaDescription;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $metaTitle;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $template;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $pageHeading;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $pageImg;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $textPreview;

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title): void
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param mixed $url
     */
    public function setUrl($url): void
    {
        $this->url = $url;
    }

    /**
     * @return mixed
     */
    public function getMeta()
    {
        return $this->meta;
    }

    /**
     * @param mixed $meta
     */
    public function setMeta($meta): void
    {
        $this->meta = $meta;
    }

    /**
     * @return mixed
     */
    public function getMetaDescription()
    {
        return $this->metaDescription;
    }

    /**
     * @param mixed $metaDescription
     */
    public function setMetaDescription($metaDescription): void
    {
        $this->metaDescription = $metaDescription;
    }

    /**
     * @return mixed
     */
    public function getMetaTitle()
    {
        return $this->metaTitle;
    }

    /**
     * @param mixed $metaTitle
     */
    public function setMetaTitle($metaTitle): void
    {
        $this->metaTitle = $metaTitle;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description): void
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getTemplate()
    {
        return $this->template;
    }

    /**
     * @param mixed $template
     */
    public function setTemplate($template): void
    {
        $this->template = $template;
    }

    /**
     * @return mixed
     */
    public function getPageHeading()
    {
        return $this->pageHeading;
    }

    /**
     * @param mixed $pageHeading
     */
    public function setPageHeading($pageHeading): void
    {
        $this->pageHeading = $pageHeading;
    }

    /**
     * @return mixed
     */
    public function getTextPreview()
    {
        return $this->textPreview;
    }

    /**
     * @param mixed $textPreview
     */
    public function setTextPreview($textPreview): void
    {
        $this->textPreview = $textPreview;
    }

    /**
     * @return Collection
     */
    public function getStaticPages(): Collection
    {
        return $this->tildaStaticPages;
    }


//    /**
//     * @param ArrayCollection $tildaStaticPages
//     */
//    public function setTildaStaticPages(ArrayCollection $tildaStaticPages): void
//    {
//        $this->tildaStaticPages = $tildaStaticPages;
//    }
    /**
     * @return mixed
     */
    public function getPageImg()
    {
        return $this->pageImg;
    }

    /**
     * @param mixed $pageImg
     */
    public function setPageImg($pageImg): void
    {
        $this->pageImg = $pageImg;
    }
}
