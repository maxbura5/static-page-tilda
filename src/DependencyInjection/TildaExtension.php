<?php

namespace meteam\TildaBundle\DependencyInjection;


use Exception;
use meteam\TildaBundle\Service\ServiceNameProvider;
use Psr\Log\LoggerInterface;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;

class TildaExtension extends Extension
{

    public function load(array $configs, ContainerBuilder $container)
    {
        $loader = new YamlFileLoader(
            $container,
            new FileLocator(__DIR__.'/../Resources/config')
        );
        try {
            $loader->load('services.yaml');
        } catch (Exception $e) {
            throw new Exception(__METHOD__ . $e->getMessage());
        }

        $configuration = new Configuration();

        $config = $this->processConfiguration($configuration, $configs);

        foreach ($config as $key => $value) {
            $container->setParameter('tilda.' . $key , $value );
        }
    }
}