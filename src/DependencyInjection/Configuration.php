<?php

namespace meteam\TildaBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder('tilda');

        $treeBuilder->getRootNode()
            ->children()
                ->scalarNode('public_key')->cannotBeEmpty()->end()
                ->scalarNode('private_key')->cannotBeEmpty()->end()
                ->scalarNode('static_page_item')->cannotBeEmpty()->end()
                ->scalarNode('static_page_list')->cannotBeEmpty()->end()
                ->arrayNode('project_ids')->prototype('scalar')->cannotBeEmpty()->end()
            ->end()->end();


        return $treeBuilder;
    }
}