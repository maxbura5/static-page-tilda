<?php

namespace meteam\TildaBundle\Form;

use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TildaStaticPageTypeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title')
            ->add('url', TextType::class,[
                'required' => true,
            ])
            ->add('meta')
            ->add('meta_description')
            ->add('meta_title')
            ->add('description')
            ->add('template')
            ->add('page_heading')
            ->add('page_img', FileType::class, [
                'mapped' => false,
                'required' => false,
            ])
            ->add('text_preview')
//            ->add('page_id')
//            ->add('project_id')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
        ]);
    }
}
