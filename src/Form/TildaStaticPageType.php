<?php

namespace meteam\TildaBundle\Form;

use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TildaStaticPageType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('locale')
            ->add('url', TextType::class,[
                'required' => true,
            ])
            ->add('body')
            ->add('meta')
            ->add('type')
            ->add('alt')
            ->add('metaDescription')
            ->add('to_show', CheckboxType::class, [
                'required' => false,
                'label'    => 'To show',
            ])
            ->add('title')
            ->add('metaTitle')
            ->add('previewImage', FileType::class, [
                'mapped' => false,
                'required' => false,
            ])
            ->add('timeToReadInMinutes')
            ->add('previewText')
//            ->add('page_id')
//            ->add('project_id')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
        ]);
    }
}
